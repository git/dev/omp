# Copyright 1999-2008 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit autotools cvs eutils flag-o-matic

PATCHSET="fvwm-2.5.26-patchset.tar.bz2"

DESCRIPTION="An extremely powerful ICCCM-compliant multiple virtual desktop window manager - Live CVS version"
HOMEPAGE="http://www.fvwm.org/"
SRC_URI="http://jesgue.homelinux.org/fvwm-files/${PATCHSET}"

LICENSE="GPL-2 FVWM"
SLOT="0"
KEYWORDS="~alpha ~amd64 ~ia64 ~ppc ~ppc64 ~sparc ~x86"
IUSE="bidi debug doc extras gtk gtk2-perl imlib netpbm nls perl png readline rplay session shape svg stroke tk translucency truetype xinerama xpm"

ECVS_SERVER="cvs.fvwm.org:/home/cvs/fvwm"
ECVS_MODULE="fvwm"
ECVS_USER="anonymous"
ECVS_PASS="guest"
ECVS_TOPDIR="${DISTDIR}/cvs-src/${ECVS_MODULE}"
S="${WORKDIR}/${ECVS_MODULE}"

RDEPEND="dev-lang/perl
	bidi? ( dev-libs/fribidi )
	gtk2-perl? (
		dev-perl/gtk2-perl
	)
	netpbm? ( media-libs/netpbm )
	perl? ( tk? (
			dev-lang/tk
			dev-perl/perl-tk
			>=dev-perl/X11-Protocol-0.56
		)
	)
	rplay? ( media-sound/rplay )
	userland_GNU? ( sys-apps/debianutils )"

DEPEND="${RDEPEND}
	dev-libs/libxslt
	dev-libs/libxml2
	dev-util/pkgconfig
	sys-libs/zlib
	x11-libs/libICE
	x11-libs/libSM
	x11-libs/libX11
	x11-libs/libXau
	x11-libs/libxcb
	x11-libs/libXcursor
	x11-libs/libXdmcp
	x11-libs/libXext
	x11-libs/libXfixes
	x11-libs/libXpm
	x11-libs/libXrandr
	x11-libs/libXrender
	x11-proto/xextproto
	x11-proto/xproto
	doc? ( dev-libs/libxslt )
	gtk? (
		=x11-libs/gtk+-1.2*
		imlib? ( media-libs/imlib )
	)
	png? ( media-libs/libpng )
	readline? (
		sys-libs/ncurses
		sys-libs/readline
	)
	stroke? ( dev-libs/libstroke )
	svg? ( gnome-base/librsvg )
	truetype? (
		media-libs/fontconfig
		virtual/xft
	)
	xinerama? (
		x11-proto/xineramaproto
		x11-libs/libXinerama
	)"

src_unpack() {

	cvs_src_unpack
	cd "${WORKDIR}"; unpack ${A}

	# Those patches have not effects until they're not used in the
	# configuration
	#EPATCH_SOURCE="${WORKDIR}/patchset"
	#EPATCH_SUFFIX="patch"
	#EXTRAS="${WORKDIR}/patchset"
	#EPATCH_FORCE="yes"

	cd "${S}"; epatch "${FILESDIR}/VerticalSeparatorMargins.patch"

	if use extras; then
		cd "${S}"; epatch "${WORKDIR}/patchset"
	fi

	# this patch enables fast translucent menus in fvwm
	# taken from: https://bugs.gentoo.org/show_bug.cgi?id=156240
	if use translucency; then
		cd "${S}"; epatch "${FILESDIR}/fvwm-2.5.23-translucent-menus.diff"
	fi

	# fixing #51287, the fvwm-menu-xlock script is not compatible
	# with the xlockmore implementation in portage.
	# This is now unconditional, since it is trivial and there is
	# no sense in not applying it.
	cd "${S}"; epatch "${FILESDIR}/fvwm-menu-xlock-xlockmore-compat.diff"

	cd "${S}"; eautoreconf
}

src_compile() {
	local myconf="--libexecdir=/usr/lib --with-imagepath=/usr/include/X11/bitmaps:/usr/include/X11/pixmaps:/usr/share/icons/fvwm --enable-package-subdirs"

	# use readline in FvwmConsole.
	if use readline; then
		myconf="${myconf} --without-termcap-library"
	fi

	local myconf="--libexecdir=/usr/lib --with-imagepath=/usr/include/X11/bitmaps:/usr/include/X11/pixmaps:/usr/share/icons/fvwm --enable-package-subdirs"

	# use readline in FvwmConsole.
	if use readline; then
		myconf="${myconf} --without-termcap-library"
	fi

	# FvwmGtk can be built as a gnome application, or a Gtk+ application.
	if ! use gtk; then
		myconf="${myconf} --disable-gtk --without-gnome"
	else
		if ! use imlib; then
			einfo "ATTN: You can safely ignore any imlib related configure errors."
			myconf="${myconf} --with-imlib-prefix=${T}"
		fi
		myconf="${myconf} --without-gnome"
	fi

	# taviso IS NOT maintainer of this ebuild
	# dont bother him about this
	export FVWM_BUGADDR="i92guboj@terra.es"

	# reccommended by upstream
	append-flags -fno-strict-aliasing

	# signed chars are required
	if use ppc; then
		append-flags -fsigned-char
	fi

	cd "${S}"
	econf ${myconf} \
		`use_enable truetype xft` \
		`use_with stroke stroke-library` \
		`use_with xpm xpm-library` \
		`use_enable nls` \
		`use_enable nls iconv` \
		`use_with png png-library` \
		`use_enable bidi` \
		`use_enable xinerama` \
		`use_enable debug debug-msgs` \
		`use_enable debug command-log` \
		`use_enable perl perllib` \
		`use_with readline readline-library` \
		`use_enable svg rsvg` \
		`use_enable session sm` \
		`use_enable shape` \
		`use_enable doc htmldoc` \
		`use_with rplay rplay-library` || die
	emake || die
}

src_install() {
	emake DESTDIR="${D}" install || die "emake install failed"

	# These are always removed, because gentoo doesn't have anymore
	# a dev-perl/gtk-perl package, so, these modules are pointless.
	rm -f "${D}/usr/share/fvwm/perllib/FVWM/Module/Gtk.pm"
	find "${D}" -name '*FvwmGtkDebug*' -exec rm -f '{}' \; 2>/dev/null

	if use perl; then
		if ! use tk; then
			rm -f "${D}/usr/share/fvwm/perllib/FVWM/Module/Tk.pm"
			if ! use gtk2-perl; then # no tk and no gtk2 bindings
				rm -f "${D}/usr/share/fvwm/perllib/FVWM/Module/Toolkit.pm"
				find "${D}/usr/share/fvwm/perllib" -depth -type d -exec rmdir '{}' \; 2>/dev/null
			fi
		fi

		# Now, the Gtk2.pm file, it will require dev-perl/gtk2-perl
		# so it implies gtk2 as well. That's why we need another use flag.
		if ! use gtk2-perl; then
			rm -f "${D}/usr/share/fvwm/perllib/FVWM/Module/Gtk2.pm"
		fi
	else
		# Compretely wipe it if ! use perl
		rm -rf "${D}/usr/bin/fvwm-perllib" \
			"${D}/usr/share/man/man1/fvwm-perllib.1"
	fi

	# Utility for testing FVWM behaviour by creating a simple window with
	# configurable hints.
	if use debug; then
		dobin "${S}/tests/hints/hints_test"
		newdoc "${S}/tests/hints/README" README.hints
	fi

	# Remove fvwm-convert-2.6 as it does not contain any code.
	rm -f "${D}/usr/bin/fvwm-convert-2.6" \
		"${D}/usr/share/man/man1/fvwm-convert-2.6.1"

	echo "/usr/bin/fvwm" > "${D}/etc/X11/Sessions/${PN}"

	dodoc AUTHORS ChangeLog NEWS README \
		docs/{ANNOUNCE,BUGS,COMMANDS,CONVENTIONS} \
		docs/{DEVELOPERS,error_codes,FAQ,TODO,fvwm.lsm}

	# README file for translucent menus patch.
	use vanilla || dodoc "${FILESDIR}/README.translucency"
}

pkg_postinst() {
	ewarn
	ewarn "This is a live CVS ebuild, it is unstable by nature, and is"
	ewarn "not related to the Gentoo Developers in any way, so don't"
	ewarn "ever report a bug about x11-wm/fvwm if you are using this"
	ewarn "ebuild. If you need support try here:"
	ewarn "http://forums.gentoo.org/viewtopic-t-465973-highlight-.html"
	ewarn
	ewarn "For information about the changes in this release, please"
	ewarn "refer to the NEWS file."
	ewarn
}
